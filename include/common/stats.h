/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h 
 * @brief fuction declarations
 *
 * @author Jose Aguas
 * @date   2018-7-3
 *
 */

#ifndef __STATS_H__
#define __STATS_H__
 

/**
  * @brief  A function that prints the statistics of an array
  *         including minimum, maximum, mean, and median.
  * @param  unsigned int minimum: print minimum
  * @param  unsigned int minimum: print maximum
  * @param  unsigned int  minimum: print mean
  * @param  unsigned int minimum: print median
  * @retval none
  */
void print_statistics(unsigned int minimum, unsigned int maximum, unsigned int mean, unsigned int median);
 
/**
  * @brief  Given an array of data and a length, 
  *         prints the array to the screen
  *
  * @param   int * ptr: Pointer to a data set
  * @param   int count: Number of items in data set
  * @retval  none
  */
void print_array(unsigned char * ptr, unsigned int count);

/**
  * @brief  Given an array of data and a length, returns the median value
  *
  * @param   int * ptr: Pointer to a data set
  * @param   int count: Number of items in data set
  * @retval  Mean of the numbers provided.
  */
unsigned int find_median(unsigned char * ptr, unsigned int count);

/**
  * @brief  Given an array of data and a length, returns the mean
  *         
  * @param   int * ptr: Pointer to a data set
  * @param   int count: Number of items in data set
  * @retval  Mean of the numbers provided.
  */
unsigned int find_mean(unsigned char * ptr, unsigned int count);

/**
  * @brief  Given an array of data and a length, returns the maximum
  *
  * @param   int * ptr: Pointer to a data set
  * @param   int count: Number of items in data set
  * @retval  Mean of the numbers provided.
  */
unsigned char find_maximum(unsigned char * ptr, unsigned int count);

/**
  * @brief  Given an array of data and a length, returns the minimum
  *         
  * @param   int * ptr: Pointer to a data set
  * @param   int count: Number of items in data set
  * @retval  Mean of the numbers provided.
  */

unsigned char find_minimum(unsigned char * ptr, unsigned int count);

/**
  * @brief  Given an array of data and a length, sorts the array 
  *         from largest to smallest.
  *         (The zeroth Element is the largest value, 
  *         and the last element (n-1) is the smallest value. )
  *
  * @param   int * ptr: Pointer to a data set
  * @param   int count: Number of items in data set
  * @retval  none
  */
void sort_array(unsigned char * ptr, unsigned int count);


#endif /* __STATS_H__ */
