#******************************************************************************
# Copyright (C) 2017 by Alex Fosdick - University of Colorado
#
# Redistribution, modification or use of this software in source or binary
# forms is permitted as long as the files maintain this copyright. Users are 
# permitted to modify this and use it to learn about the field of embedded
# software. Alex Fosdick and the University of Colorado are not liable for any
# misuse of this material. 
#
#*****************************************************************************

#------------------------------------------------------------------------------
#	C1M2 Target examples:
#	make memory.o	PLATFORM=HOST
#	make build 		PLATFORM=HOST COURSE=COURSE1
#	make compile-all 	PLATFORM=MSP432
#	make clean	 	PLATFORM=MSP432
#------------------------------------------------------------------------------
include sources.mk

COURSE=COURSE1

ifndef PLATFORM
    $(error "PLATFORM is not defined.")
endif

ifdef ($(PLATFORM),)
    $(error "PLATFORM device is not defined.")
endif

# Platform Overrides
ifeq ($(PLATFORM),HOST)

   SRCS_ASM = $(SRCS_ASM_HOST)
   SRCS_PRE = $(SRCS_PRE_HOST)
   SRCS = $(SRCS_HOST)

   CC=gcc
   CXX=g++
   AR=ar
   AS=as
   LD=ld
   NM=nm
   OBJDUMP=objdump
   RANLIB=ranlib
   STRIP=strip

   LDFLAGS = -Wl,-Map=build/$(TARGET).map

   CFLAGS = -Wall -Werror -g -O0 -std=c99 -DHOST -D$(COURSE)


else
   GCC_BIN_DIR ?= ./gcc-arm-none-eabi-7-2018-q2-update/bin/
#   GCC_BIN_DIR ?= ../bin/
   GCC_INC_DIR ?= ./gcc-arm-none-eabi-7-2018-q2-update/arm-none-eabi/include

   SRCS_ASM = $(SRCS_ASM_TARGET)
   SRCS_PRE = $(SRCS_PRE_TARGET)
   SRCS = $(SRCS_TARGET)

   # Architectures Specific Flags
   LINKER_FILE = ./msp432p401r.lds
   CPU = cortex-m4
   ARCH = thumb
   SPECS = nosys.specs

   # Compiler Flags and Defines
   CC = $(GCC_BIN_DIR)arm-none-eabi-gcc
   CXX = $(GCC_BIN_DIR)arm-none-eabi-g++
   AR = $(GCC_BIN_DIR)arm-none-eabi-ar
   AS = $(GCC_BIN_DIR)arm-none-eabi-as
   LD = $(GCC_BIN_DIR)arm-none-eabi-ld
   NM = $(GCC_BIN_DIR)arm-none-eabi-nm
   OBJDUMP = $(GCC_BIN_DIR)arm-none-eabi-objdump
   RANLIB = $(GCC_BIN_DIR)arm-none-eabi-ranlib
   STRIP = $(GCC_BIN_DIR)arm-none-eabi-strip

   LDFLAGS = -Wl,-Map=build/$(TARGET).map -T $(LINKER_FILE)
   CFLAGS = -mcpu=$(CPU) -m$(ARCH) -march=armv7e-m -mfloat-abi=hard -mfpu=fpv4-sp-d16 --specs=$(SPECS) -Wall -Werror -g -O0 -std=c99 -DMSP432 -D$(COURSE)
endif

TARGET = course1

OBJS = $(SRCS_ASM:.asm=.o)

ASM = $(SRCS_PRE:.i=.asm)
PRE = $(SRCS:.c=.i)

.PHONY: help
help:
	@echo  'C1M2 Target examples:'
	@echo  '    make memory.o	PLATFORM=HOST'
	@echo  '    make build 		PLATFORM=HOST'
	@echo  '    make compile-all 	PLATFORM=MSP432'
	@echo  '    make clean	 	PLATFORM=MSP432'

.PHONY: compile-all
compile-all:  $(PRE) $(ASM) $(OBJS)

.PHONY: build
build: all

.PHONY: all
all: $(TARGET).out

$(TARGET).out: dobuild $(PRE) $(ASM) $(OBJS)
	@echo '   ' $(OBJS) 'object files files'
	$(CC) $(shell echo build/*.o) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) -o build/$@
	@echo "Generated output files are in build directory!!!"
	@echo "Generated output files are in build directory!!!"

%.o : src/%.c
	$(CC) -c $< $(CFLAGS) $(INCLUDES) -o build/$@
	$(CC) -MM $< $(CFLAGS) $(INCLUDES)  > build/$*.d


%.asm : src/%.c
	$(CC) -S $< $(CFLAGS) $(INCLUDES) -o build/$@

%.i : src/%.c
	$(CC) -E $< $(CFLAGS) $(INCLUDES) -o build/$@

dobuild:
	@echo '   ' $(PRE) 'prepocessor files'
	@echo '   ' $(ASM) 'assembly files'
	@echo '   ' $(OBJS) 'object files files'
	-@if [ ! -d build ] ; \
	  then \
	    mkdir build ; \
	  fi

.PHONY: clean
clean:
	rm -rf *~
	rm -rf build



