/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/

#include <stdint.h>
#include "course1.h"
#include "platform.h"
#include "memory.h"
#include "data.h"
#include "stats.h"
/*
 * Integer-to-ASCII needs to convert data from a standard integer type 
 *    into an ASCII string.
 * All operations need to be performed using pointer arithmetic, 
 *    not array indexing
 * The number you wish to convert is passed in as a signed 32-bit integer.
 * You should be able to support bases 2 to 16 by specifying 
 *   the integer value of the base you wish to convert to (base).
 * Copy the converted character string to the uint8_t* pointer passed 
 *   in as a parameter (ptr)
 * The signed 32-bit number will have a maximum string size 
 *   (Hint: Think base 2).
 * You must place a null terminator at the end of the converted c-string
 * Function should return the length of the converted data 
 *   (including a negative sign). Example my_itoa(ptr, 1234, 10) should 
 *   return an ASCII string length of 5 (including the null terminator).
 * This function needs to handle signed data.
 * You may not use any string functions or libraries
*/
uint8_t my_itoa(int32_t data, uint8_t * ptr, uint32_t base){

    uint8_t length = 0; 
    uint8_t  sign = 0;
    int rem; 
 
if (data == 0){ // Empty string?
*(ptr++) = '0';
  *ptr = '\0'; 
length++;
        return length; 
    } 

if (data < 0){ // Negative?
        sign = 1; 
        data = -data; 
    } 
  
    // Processing individual digits... 
    while (data != 0) { 
//PRINTF("  data: %d\n", data);
        rem = data % base; 
//PRINTF("  rem: %d\n", rem);
        *ptr = (rem > 9)? (rem-10) + 'a' : rem + '0'; 
//PRINTF("  digit: %c\n", *ptr);
ptr++;
        data = data/base; 
	length++;
    } 
  
 
    if (sign){ 
      *(ptr++) = '-'; // If negative, append '-'
      length++;
//PRINTF("  negative \n");
    }
  
    *ptr = '\0'; // Append string terminator

   PRINTF("  length: %d\n", length);
my_reverse(ptr-(length), length); // Reverse the string
  
return length;

}


/*
 * ASCII-to-Integer needs to convert data back from an ASCII 
 *    represented string into an integer type.
 * All operations need to be performed using pointer arithmetic, 
 *    not array indexing
 * The character string to convert is passed in as a uint8_t * pointer (ptr).
 * The number of digits in your character set 
 *    is passed in as a uint8_t integer (digits).
 * You should be able to support bases 2 to 16.
 * The converted 32-bit signed integer should be returned.
 * This function needs to handle signed data.
 * You may not use any string functions or libraries
*/
int32_t my_atoi(uint8_t * ptr, uint8_t digits, uint32_t base){
  int32_t int_value=0;
  int32_t sign=1;
  int32_t index=0;

if(*(ptr) == '-'){
    sign = -1;
    ptr++;
    index++;
//PRINTF("  negative \n");
  }

  for(;  index < digits; index++){
//PRINTF("  value: %d\n", int_value);
int_value= int_value*base + *(ptr) -'0';
    ptr++;
  }

  return sign*int_value;
}
