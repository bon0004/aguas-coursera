/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c 
 * @brief analyzes an array of unsigned char data items 
 *        and reports analytics on the maximum, minimum, mean, 
 *        and median of the data set. In addition, 
 *        reorders the data set from large to small.
 *
 * @author Jose Aguas
 * @date   2018-7-3
 *
 */



#include <stdio.h>
#include "stats.h"
#include "platform.h"

/* Size of the Data Set */
#define SIZE (40)

void main() {

  unsigned int minimum, maximum, mean, median;

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};
  
#if VERBOSE
  print_array(test,SIZE); 
#endif

  sort_array(test,SIZE);

#if VERBOSE
  print_array(test,SIZE);
#endif

  maximum=find_maximum(test, SIZE);
  minimum=find_minimum(test, SIZE);
  mean=find_mean(test, SIZE);
  median=find_median(test, SIZE);
  print_statistics(minimum, maximum, mean, median);

}

/**
  * @brief  A function that prints the statistics of an array
  *         including minimum, maximum, mean, and median.
  *
  * @param  int minimum: print minimum
  * @param  int minimum: print maximum
  * @param  int minimum: print mean
  * @param  int minimum: print median
  * @retval none
  */
void print_statistics(unsigned int minimum, unsigned int maximum, unsigned int mean, unsigned int median){
  printf(" minimum %i\n", minimum );
  printf(" maximum %i\n", maximum );
  printf(" mean %i\n", mean );
  printf(" median %i\n", median );
} 

/**
  * @brief  Given an array of data and a length, 
  *         prints the array to the screen
  *
  * @param   int * ptr: Pointer to a data set
  * @param   int count: Number of items in data set
  * @retval  none
  */
void print_array(unsigned char * ptr, unsigned int count){
  unsigned int i;

  printf("\nArray elements : \n");

  for (i = 0; i < count; i++){
    printf("%d ", *ptr);
    ptr++;
  }
   printf("\n\n"); 
}

/**
  * @brief  Given an array of data and a length, returns the median value
  *
  * @param   int * ptr: Pointer to a data set
  * @param   int count: Number of items in data set
  * @retval  Mean of the numbers provided.
  */
unsigned int find_median(unsigned char * ptr, unsigned int count){
  unsigned int median;
  median = count;
  median = ((median + 1)/2) -1;

  return (unsigned int)ptr[median];
}
/**
  * @brief  Given an array of data and a length, returns the mean
  *         
  * @param   int * ptr: Pointer to a data set
  * @param   int count: Number of items in data set
  * @retval  Mean of the numbers provided.
  */
unsigned int find_mean(unsigned char * ptr, unsigned int count){
  int i;
  unsigned int mean=0;

  if ( ptr == NULL){
    return 0;
  }
  
  if ( count <= 0 ) {
    count = 1;
  }
  
  for(i = 0; i < count; i++){
    mean += *ptr;
    ptr++;
  }
 
  return (mean / count);
}

/**
  * @brief  Given an array of data and a length, returns the maximum
  *
  * @param   int * ptr: Pointer to a data set
  * @param   int count: Number of items in data set
  * @retval  Mean of the numbers provided.
  */
unsigned char find_maximum(unsigned char * ptr, unsigned int count){
return (unsigned int)ptr[0];
}

/**
  * @brief  Given an array of data and a length, returns the minimum
  *         
  * @param   int * ptr: Pointer to a data set
  * @param   int count: Number of items in data set
  * @retval  Mean of the numbers provided.
  */
unsigned char find_minimum(unsigned char * ptr, unsigned int count){
return (unsigned int)ptr[count-1];
}

/**
  * @brief  Given an array of data and a length, sorts the array 
  *         from largest to smallest.
  *         (The zeroth Element is the largest value, 
  *         and the last element (n-1) is the smallest value. )
  *
  * @param   int * ptr: Pointer to a data set
  * @param   int count: Number of items in data set
  * @retval  none
  */
void sort_array(unsigned char * ptr, unsigned int count){
  int i, j, m;  
  unsigned tmp;
  unsigned msb[count];

    for (i = 0; i < count; i++)
	msb[i] = ptr[i];

    for (i = 0; i < count-1; i++){
      m=i;  
      for(j=i+1; j < count; j++){
	if(msb[j] < msb[m]) m = j;
      }
      tmp=msb[i];
      msb[i]=msb[m];
      msb[m]=tmp;
    }

    tmp= count -1;
    for (i = 0; i < count; i++){
      ptr[i]=msb[tmp - i];
    }
}




 
